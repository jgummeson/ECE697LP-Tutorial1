# Tutorial 1: Implementing a pedometer using the Thingy

In this tutorial, you will be using the Motion Processing Unit (MPU9250) inside the Thingy52 to implement the functionality of a pedometer using the 3-axis accelerometer inside the MPU.

Consult the [firmware documentation](https://nordicsemiconductor.github.io/Nordic-Thingy52-FW/documentation/index.html) for details regarding the firmware this project was based on.

## Prerequisites

In order to visualize sensor data, you need to install gnuplot, which isn't in the VM you installed in tutorial 0.5 (or tutorial 0). To install this, run this command from the Ubuntu terminal:

sudo apt-get install gnuplot

After installing gnuplot, you need to download the starter code for the assignment, which is available for download using this command from your home directory.

Don't forget to update Makefile.windows or Makefile.posix in sdk_components/toolchain/gcc according to installation instructions (i.e. update the path to the Arm GCC toolchain)

## Code Walkthrough

This repository is a heavily modified version of the Thingy development environment we will be using for this tutorial. Navigate to project/pca20020_s132. If you see 'main.c' you are in the right place.

At line 332 you will see the definition of the main function. The first several lines of this function initialize a number of Thingy subsystems that will be used in this tutorial.  

The first line of interest initializes the logging system.

`(332) err_code = NRF_LOG_INIT(NULL);`

The nRF log tool uses the JLink Real Time Terminal (RTT) functionality in your JLink programmer to output useful debugging information over the SWD interface that you can view as your code executes. This is a highly efficient way of debugging code as our application only needs to write to a memory buffer, and the RTT hardware directly reads from these memory locations and outputs on a virtual serial port without needing to involve the microcontroller. The next lines of interest are:
`APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);`
`err_code = app_timer_init();`
`APP_ERROR_CHECK(err_code);`
`board_init();`
`thingy_init();`
`raw_motion_enable();`

The first line initializes a scheduler that allows the Thingy to schedule and run different code sections at the boundaries of system clock ticks. The next line initializes the Thingy's virtual timer subsystem that allows multiple timers to be derived from one of the microcontrollers hardware timers. This will be used in the second part of the tutorial. The next three lines initialize different aspects of the hardware. board_init() takes care of low level GPIO initialization, thingy_init() initializes sensor and actuator hardware, as well as the bluetooth stack, while raw_motion_enable() initializes the MPU in a mode where it outputs raw 3-axis accelerometer data. To understand more about what raw_motion_enable() does, let's take a look at a header file, m_motion.h and its implementation m_motion.c, that was included at the top of main.c.
The source file m_motion.c can be found inside source/modules (assuming you are inside the outermost folder that was downloaded by git). Similarly, the header file can be found inside inc/modules. Inside the .c file, we can see the definition of this function in turn calls another low level driver function at line 193:

`err_code = drv_motion_enable(DRV_MOTION_FEATURE_MASK_RAW_ACCEL);`

The driver function is defined inside drv_motion.c (src/drivers) and is included using drv_motion.h (include/drivers). Exploring the details of this driver is a bit outside the scope of this tutorial, but we can pass several different arguments to this function, one of which configures registers to instruct the MPU to output raw accelerometer data. The MPU is a very sophisticated device that contains an accelerometer, gyroscope, magnetometer, and has internal processing to convert raw motion data into features including quarternians and detected footsteps. After this accelerometer starts, it passes data to m_motion.c that we looked at earlier at line 82:

`static void drv_motion_evt_handler(drv_motion_evt_t const * p_evt, void * p_data, uint32_t size)`

Several pieces of information are passed to this tutorial including the data type being passed, the amount of data, and a pointer to the data. Under the hood, firmware that is part of the SDK is fetching these readings from a hardware FIFO buffer on the MPU whenever an interrupt indicates data is ready. This means that the microcontroller will be triggered every time a new reading is generated. The accelerometer has been pre-configured to output data at 25 Hz, so this function will get called 25 times a second to handle data as it arrives. Inside this function we can see a case statement that handles the raw data, and another which process footstep data. First, we will be looking at the raw data.
The data initially arrives in a fixed point format and gets converted to a double floating point format that is easier to work with for some signal processing routines (though it is possible to work directly with fixed point to save power!) Lines 112 and 113 output this data to our terminal in a comma separated format:

`sprintf(buffer, "ACCEL_DATA %.2f,%.2f,%.2f", x_buf,y_buf,z_buf);`
`NRF_LOG_INFO(" %s\r\n", buffer);`

We will use this data for visualization later, but in order for us to implement a pedometer, we need to clean up the data to make sense of it, so line 114 calls the function where we will be doing the bulk of our work for this exercise:

`process_accel_data(x_buf,y_buf,z_buf);`

There are a few notable things happening in this function, the first of which happens at line 231 which performs the function:

`magnitude = SquareRoot(X^2 + Y^2 + Z^2)`

This operation removes the directional dependance of the accelerometer so that we can track vertical motions caused by footsteps irrespective of the devices orientation.
Next lines 233 - 237 normalize the data and remove the DC bias caused by gravity, which will be distributed amongst the 3-axes depending on the orientation:

```
//compute the mean acceleration across the window
for(int i = 0; i < ACCEL_BUF_SIZE; i++)
    mean_acc = mean_acc + mag_buffer[i];
    mean_acc = mean_acc / ACCEL_BUF_SIZE;
```

One of the design tradeoffs we have is the size of the buffer we use to perform averaging (ACCEL_BUF_SIZE). A larger buffer allows us to compute a better average over a longer time window, but takes longer to computer.
After this step, the data is output with a tag "NORM_DATA" for later visualization, using JLink RTT.

The next step smooths out small signal variations in our normalized accelerometer data using an exponentially waiting average or EWMA. An EWMA is a simplified way of performing the signal processing function of a low pass filter:
`y[t+1] = alpha*y[t] + (1-alpha)*y[t-1]`

Explanation of the theory of this filter is beyond the scope of the tutorial but the cutoff frequency of the filter is ultimately defined as:

`fcutoff  = (alpha) / ((1-alpha)*2pi*delta_t)`

Where delta t is the sensors sampling period (1 / 25 Hz). The output of this data is also output to the RTT terminal using the tag using the tag EWMA_DATA.
The final part of the signal processing pipeline exploits the fact that vertical motions during walking looks roughly like a sine wave after the previous steps using by counting zero crossings of the signal. Lines 257 - 278 implement this function (eliminated several commented lines:

```
if(normalized_accel > ACCEL_HYST)
    {
      //if previous state was a negative sign below hysteresis window, step detected
      if(sign_state_pos == false)`
      {
        sprintf(buffer, "STEP_DETECTED %i\r\n", step_count);
        NRF_LOG_INFO(" %s\r\n", buffer);
        step_count++;
        new_step = true;
      }
        sign_state_pos = true;`
    }`
    else if(normalized_accel < (0-ACCEL_HYST))
    {
      sign_state_pos = false;
    }    if(normalized_accel > ACCEL_HYST)
    {
      //if previous state was a negative sign below hysteresis window, step detected
      if(sign_state_pos == false)
      {
        sprintf(buffer, "STEP_DETECTED %i\r\n", step_count);
        NRF_LOG_INFO(" %s\r\n", buffer);
        step_count++;
        new_step = true;
      }
        sign_state_pos = true;
    }
    else if(normalized_accel < (0-ACCEL_HYST))
    {
      sign_state_pos = false;
    }
```

This simple algorithm keeps track of the history of the sign of the signal using a single binary value. Each time the smoothed data is updated, we compare the current value outputted from the EWMA step and see if it is above or below  a value defined by a floating point constant "ACCEL_HYST. If below the negative value, we update the "sign_state_pos" value to false and if it is above the positive value we output to true. If the previous state was "false" and the new step was "true" we determine that a step has occurred, increment a variable and output it to the console. This hysteresis window makes the step counter more robust against noise around the zero point of the signal and ensures that only larger motions will register a step.

